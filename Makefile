CC = gcc
LD = gcc
CFLAGS = -std=c99
objects = main.o bmp_handler.o bmp_util.o

BitmapBanging: $(objects)
	$(LD) -o BitmapBanging $^
	
main.o: src/main.c
	$(CC) -c $^ -o $@ $(CFLAGS)

bmp_handler.o: src/bmp_handler.c
	$(CC) -c $^ -o $@ $(CFLAGS)

bmp_util.o: src/bmp_util.c
	$(CC) -c $^ -o $@ $(CFLAGS)

clean: 
	rm $(objects)
