#ifndef BMP_CONSTANTS_H
#define BMP_CONSTANTS_H

#include "bmp_structs.h"

#define BI_RGB       0
#define BI_RLE8      1
#define BI_RLE4      2
#define BI_BITFIELDS 3
#define BI_JPEG      4
#define BI_PNG       5

#define FILE_HEADER_SIZE sizeof(bitmapHeader)
#define INFO_HEADER_SIZE sizeof(bitmapInfoHeader)
#define RGB_QUAD_SIZE sizeof(rgbQuad)
#define RGB_TRIP_SIZE sizeof(rgbTriple)

#endif