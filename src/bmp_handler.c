#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include <string.h>     // for memcpy
#include "bmp_handler.h"

//-- private variables
static bitmapHeader bmph;
static bitmapInfoHeader bmpih;
static rgbQuad* pallet = 0;
static rgbTriple* canvas = 0;
static uint32_t palletSize;
static uint64_t canvasSize;
static uint8_t linePadding;

//-- getter functions
rgbTriple* getCanvasCopy ( void ) {
    rgbTriple* copy = malloc(canvasSize);
    return(memcpy(copy, canvas, canvasSize));
}

char *   getSignature       ( void ) { return(bmph.bfType);          }
uint32_t getSize            ( void ) { return(bmph.bfSize);          }
uint32_t getOffset          ( void ) { return(bmph.bfOffBits);       }
int32_t  getWidth           ( void ) { return(bmpih.biWidth);        }
int32_t  getHeight          ( void ) { return(bmpih.biHeight);       }
uint32_t getHeaderSize      ( void ) { return(bmpih.biSize);         }
uint16_t getBitsPerPixel    ( void ) { return(bmpih.biBitCount);     }
uint32_t getCompression     ( void ) { return(bmpih.biCompression);  }
uint32_t getImageSize       ( void ) { return(bmpih.biSizeImage);    }
uint32_t getUsedColors      ( void ) { return(bmpih.biClrUsed);      }
uint32_t getImportantColors ( void ) { return(bmpih.biClrImportant); }
uint16_t getPalletSize      ( void ) { return(palletSize/sizeof(rgbQuad)); }

//-- print functions
void printPallet( void ) {
    for ( int i = 0; i < getPalletSize(); i++ ) {
        rgbQuad tmp = *(pallet + i);
        printf("i: %d \tr: %d\tg: %d\tb: %d\n\r", i, tmp.red, tmp.green, tmp.blue);
    }
}

//-- load functions
/* raw read 16, 24, 32 bpp */
static int rawRead ( FILE* bmpFile ) {
    int32_t width = getWidth();
    int32_t height = getHeight();
    uint16_t bpp = getBitsPerPixel();
    uint16_t bytePerPixel = bpp/8;
    uint32_t bytePerLine = bytePerPixel * width;
    linePadding = (4-(bytePerLine%4))%4;

    for ( int y = 0; y < height; y++ ) {
        for ( int x = 0; x < width; x++ ) {
            rgbTriple* tempPointer = canvas+(y*width+x);
            switch ( bpp ) {
                case 24 :
                    fread(tempPointer, RGB_TRIP_SIZE, 1, bmpFile);
                break;
            }
        }
        fseek(bmpFile, linePadding, SEEK_CUR);
    }
    return(0);
}

/* pallet read 1, 2, 4, 8 bpp */
static int palletRead ( FILE* bmpFile ) {
    int32_t width = getWidth();
    int32_t height = getHeight();
    uint16_t bpp = getBitsPerPixel();
    uint16_t bytePerPixel = bpp/8;
    uint32_t bytePerLine = bytePerPixel * width;
    linePadding = (4-(bytePerLine%4))%4;
    uint8_t index = 0;

    for ( int y = 0; y < height; y++ ) {
        for ( int x = 0; x < width; x++ ) {
            rgbTriple* tempPointer = canvas+(y*width+x);
            switch ( bpp ) {
                case 8 :
                    fread(&index, 1, 1, bmpFile);
                    rgbQuad tmpPall = *(pallet + index);
                    *tempPointer = (rgbTriple){
                        tmpPall.blue,
                        tmpPall.green,
                        tmpPall.red,
                    };
                    //printf("x: %d \ty: %d, \tr: %d, \tg: %d, \tb: %d, \ti: %d\n", x, y, tmpPall.red, tmpPall.green, tmpPall.blue, index);
                    //printf("%d\n", index);
                break;
            }
        }
        fseek(bmpFile, linePadding, SEEK_CUR);
    }
    return(0);
}

/* rle read 4, 8 bpp */
static int rleRead ( FILE* bmpFile ) {
    //uint16_t bpp = getBitsPerPixel();
    //uint32_t imageSize = getImageSize();
    int32_t width = getWidth();
    int32_t x = 0;
    int32_t y = 0;
    uint8_t i = 0;
    uint8_t temp = 0;
    rgbQuad tempquad;
    /* read cpntrol byte in loop head*/
    while ( fread(&temp, 1, 1, bmpFile) ) {
        if ( temp == 0 ) {
            /* read instrcution byte */
            fread(&temp, 1, 1, bmpFile);
            switch ( temp ) {
                /* end of line */
                case 0 :
                    y++;
                    x = 0;
                    break;
                /* end of canvas */
                case 1 :
                    return(0); 
                    break;
                /* delta */
                case 2 :
                    fread(&temp, 1, 1, bmpFile);
                    x += temp;
                    fread(&temp, 1, 1, bmpFile);
                    y += temp;
                    break;
                /* free write */
                default :
                    /* amaount of bytes */
                    i = temp;
                    for ( ; i > 0; i-- ){
                        fread(&temp, 1, 1, bmpFile);
                        tempquad = (*(pallet+temp));
                        *(canvas+(y*width+x)) = (rgbTriple){tempquad.blue, tempquad.green, tempquad.red};
                        x++;
                    }
                    /* fill to word boundaries */
                    fseek(bmpFile, ftell(bmpFile)%2, SEEK_CUR);
                    break;
            }
        } else {
            /* save multiplier */
            i = temp;
            /* read color index */
            fread(&temp, 1, 1, bmpFile);
            tempquad = (*(pallet+temp));
            for ( ; i > 0; i-- ){
                //printf("%d\n", i);
                *(canvas+(y*width+x)) = (rgbTriple){tempquad.blue, tempquad.green, tempquad.red};
                x++;
            }
        }
    }
    return(0);
}

/* general read function, calling other functions */
static int readToCanvas ( FILE* bmpFile ) {
    /* prepare cnavas and file stream */
    canvasSize = getWidth() * getHeight() * RGB_TRIP_SIZE;
    canvas = malloc( canvasSize );
    /* go to start of pixel matrix */
    fseek(bmpFile, getOffset(), SEEK_SET);
    switch ( getCompression() ) {
        case BI_RGB : 
            switch ( getBitsPerPixel() ) {
                case 1 :
                case 2 :
                case 4 :
                case 8 : palletRead(bmpFile); break;
                case 16 :
                case 24 : rawRead(bmpFile); break;
            }
            break;
        case BI_RLE4 : 
        case BI_RLE8 : rleRead(bmpFile); break;
        default: return(-1); break;
    }
    return(0);
}

static int readToPallet ( FILE* bmpFile ) {
    /* determine w size */
    uint16_t bpp = getBitsPerPixel();
    if ( ( 1 <= bpp ) && ( bpp <= 8 ) ) {
        /* if UsedColor is 0, pallet is maximum size */
        if ( getUsedColors() == 0 ) {
            palletSize = RGB_QUAD_SIZE * ( 1 << bpp );
            // alternative :
            // palletSize = RGB_QUAD_SIZE * pow(2, bpp);
        }
        else {
            palletSize = RGB_QUAD_SIZE * getUsedColors();
        }
    }
    pallet = malloc( palletSize );
    /* go to pallet start in file stream */
    fseek(bmpFile, sizeof(bitmapHeader)+getHeaderSize(), SEEK_SET);
    /* read pallet in one rush */
    if ( !fread(pallet, palletSize, 1, bmpFile) ) { return (-1); }
    return(0);
}

int readBMPfromFile ( FILE* bmpFile ) {
    /* read file header */
    if ( !fread(&bmph, sizeof(bitmapHeader), 1, bmpFile) ) { return (-1); }
    /* check for magic bytes */
    if ( ! ( bmph.bfType[0] == 'B' && bmph.bfType[1] == 'M' ) ) { return(-1); }
    /* read info */
    if ( !fread(&bmpih, sizeof(bitmapInfoHeader), 1, bmpFile) ) { return (-1); }
    /* read pallet */
    if ( readToPallet ( bmpFile ) < 0 ) { return (-1); }
    /* read actual pixel matrix */
    readToCanvas( bmpFile );

    return(0);
}


void setNewCanvas( rgbTriple* newCanvas ){
    free(canvas);
    canvas = newCanvas;
}

int saveBMPtoFile( FILE* bmpFile ){
    bitmapHeader fileHeader = (bitmapHeader) {
        {'B', 'M'},
        FILE_HEADER_SIZE + INFO_HEADER_SIZE + canvasSize,
        0,
        0,
        FILE_HEADER_SIZE + INFO_HEADER_SIZE
    };
    bitmapInfoHeader infoHeader = (bitmapInfoHeader) {
        INFO_HEADER_SIZE,
        getWidth(),
        getHeight(),
        1,
        24,
        BI_RGB,
        getHeight()*getWidth()*3,
        0,
        0,
        0,
        0
    };
    fwrite(&fileHeader, sizeof(fileHeader), 1, bmpFile);
    fwrite(&infoHeader, sizeof(infoHeader), 1, bmpFile);

    int32_t width = getWidth();
    int32_t height = getHeight();
    uint32_t bytePerLine = 3 * width;
    linePadding = (4-(bytePerLine%4))%4;
    for ( int y = 0; y < height; y++ ) {
        for ( int x = 0; x < width; x++ ) {
            rgbTriple* tempPointer = canvas+(y*width+x);
            fwrite(tempPointer, RGB_TRIP_SIZE, 1, bmpFile);
        }
        fseek(bmpFile, linePadding, SEEK_CUR);
    }
    return(0);
}

void cleanUpBMP ( void ) {
    if ( pallet != 0 ) {
        free(pallet);
        pallet = 0;
    }
    if ( canvas != 0 ) {
        free(canvas);
        canvas = 0;
    }
}