#ifndef BMP_HANDLER_H
#define BMP_HANDLER_H

#include "bmp_structs.h"
#include "bmp_constants.h"

int readBMPfromFile( FILE* bmpFile );
void printPallet( void );
void setNewCanvas( rgbTriple* canvas );
int saveBMPtoFile( FILE* bmpFile );
rgbTriple* getCanvasCopy( void );
void cleanUpBMP ( void );

char*    getSignature       ( void );
uint32_t getSize            ( void );
uint32_t getOffset          ( void );
int32_t  getWidth           ( void );
int32_t  getHeight          ( void );
uint32_t getHeaderSize      ( void );
uint16_t getBitsPerPixel    ( void );
uint32_t getCompression     ( void );
uint32_t getImageSize       ( void );
uint32_t getUsedColors      ( void );
uint32_t getImportantColors ( void );
uint16_t getPalletSize      ( void );

#endif