#ifndef BMP_STRUCTS_H
#define BMP_STRUCTS_H

#pragma pack(push, 1)
typedef struct {
    char     bfType[2];
    uint32_t bfSize;
    uint16_t bfReserved1;
    uint16_t bfReserved2;
    uint32_t bfOffBits;
} bitmapHeader;

typedef struct {
    uint32_t biSize;
    int32_t  biWidth;
    int32_t  biHeight;
    uint16_t biPlanes;
    uint16_t biBitCount;
    uint32_t biCompression;
    uint32_t biSizeImage;
    int32_t  biXPelsPerMeter;
    int32_t  biYPelsPerMeter;
    uint32_t biClrUsed;
    uint32_t biClrImportant;
} bitmapInfoHeader;

typedef struct {
    uint8_t blue;
    uint8_t green;
    uint8_t red;
    uint8_t reserved;
} rgbQuad;

typedef struct {
    uint8_t rgbtBlue;
    uint8_t rgbtGreen;
    uint8_t rgbtRed;
} rgbTriple;

typedef struct{
    bitmapHeader fileHeader;
    bitmapInfoHeader infoHeader;
    rgbQuad* pallet;
    rgbTriple* canvas;
} bitmap_t;
#pragma pack(pop)

#endif