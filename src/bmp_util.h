#ifndef BMP_UTIL_H
#define BMP_UTIL_H
#include "bmp_structs.h"
typedef struct {
    rgbTriple color;
    int32_t xMin;
    int32_t xMax;
    int32_t yMin;
    int32_t yMax;
} rectangleInfo;

rectangleInfo* getRectangles(void);
rgbTriple* markRectangles ( void );
void canvasTest(void);
int checkRectangles(void);
int drawRectangles(rectangleInfo* rectangleArray, int size);


#endif 
