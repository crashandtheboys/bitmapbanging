#include <stdio.h>
#include <stdint.h>
#include <string.h>
#include "bmp_handler.h"
#include "bmp_util.h"

//-- private functions
static void printBitmapInfo() {
    printf("Signature    : %c%c\n\r", getSignature()[0], getSignature()[1]);
    printf("File Size    : %d byte\n\r", getSize());
    printf("Matrix Start : 0x%x\n\r", getOffset());
    printf("Header Size  : %d byte\n\r", getHeaderSize());
    printf("Width        : %d\n\r", getWidth());
    printf("Height       : %d\n\r", getHeight());
    printf("BitsPerPixel : %d\n\r", getBitsPerPixel());
    printf("Compression  : %d\n\r", getCompression());
    printf("Image Size   : %d byte\n\r", getImageSize());
    printf("Used Colors  : %d (0 means all)\n\r", getUsedColors());
    printf("Imp. Colors  : %d (0 means all)\n\r", getImportantColors());
    printf("Pallet Size  : %d entries\n\r", getPalletSize());
    //printPallet();
}

//-- main
int main( int argc, char* argv[] ) {
    /* first argument is input file name */
    char* inputFileName = argv[1];
    /* output file name extension */
    char* extension = "_output.bmp";
    /* calculate out file name length */
    int outputFileNameLength = strlen(inputFileName) + strlen(extension);
    char outputFileName[outputFileNameLength + 1];
    /* copy other strings */
    strcpy(outputFileName, inputFileName);
    strcat(outputFileName, extension);
    /* zero terminated string */
    outputFileName[outputFileNameLength] = 0;

    printf("\n\r");
    
    /* assert correct number of arguments */
    if ( argc != 2 ) {
        printf("Program takes exacly one argument. Exiting.\n\r");
        return(-1);
    }

    /* open input file */
    printf("Opening %s\n\r\n\r", argv[1]);
    FILE* inputFile = fopen(inputFileName,"rb"); 
    if ( inputFile == NULL ) {
        printf("Could not open input file. Exiting.\n\r");
        return(-1);
    }

    /* open output file */
    printf("Creating output file %s\n\r\n\r", argv[1]);
    FILE* outputFile = fopen(outputFileName,"wb"); 
    if ( outputFile == NULL ) {
        printf("Could not create output file. Exiting.\n\r");
        return(-1);
    }

    //-- INPUT
    /* chech if bitmap */
    if ( "%d", readBMPfromFile(inputFile) == -1 ) {
        printf("File is not bitmap. Exiting.\n\r");
        return(-1);
    }

    //-- PROCESSING
    printBitmapInfo();
    setNewCanvas(markRectangles());

    //-- OUTPUT
    saveBMPtoFile(outputFile);
    fclose(inputFile);
    fclose(outputFile);

    cleanUpBMP();
    printf("\n\r");

    return(0);
}
