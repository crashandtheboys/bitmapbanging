#include <inttypes.h>
#include <stdio.h>
#include <stdint.h>
#include <stdlib.h>
#include "bmp_util.h"
#include "bmp_handler.h"

//--static function prototypes
static void printInfo(void);
static int drawHorizontalLine(int start, int end, int height );
static int drawVerticalLine(int start, int end, int xValue);
static rectangleInfo* checkForRectangle(int x0, int y0);
static int compareColor( rgbTriple first, rgbTriple second );
static int sameColorCoordinates( int x, int y, rgbTriple color );

//variables
rectangleInfo* rectangles;

//get canvas 
rgbTriple* canvas;
uint32_t canvasToBeAlteredSize;

static int drawHorizontalLine(int start, int end, int y ) {
    for(int i = start; i <= end; i++) {
      *(canvas+(y*getWidth()+i)) = (rgbTriple){ 255, 0, 0};
    }
}

static int drawVerticalLine(int start, int end, int x) {
    for(int i = start; i <= end; i++) {
        *(canvas+(i*getWidth()+x)) = (rgbTriple) {255,0,0};    
    }
}

static int compareColor( rgbTriple first, rgbTriple second ) {
    return( 
        ( first.rgbtBlue == second.rgbtBlue ) &&
        ( first.rgbtGreen == second.rgbtGreen ) &&
        ( first.rgbtRed == second.rgbtRed )
        );
}

static int sameColorCoordinates( int x, int y, rgbTriple color ) {
    if ( x < 0 || x >= getWidth() || y < 0 || y >= getHeight() ){
        return(0);
    } else {
        return(compareColor(*(canvas+(y*getWidth()+x)), color));
    }
}

static rectangleInfo* checkForRectangle ( int x0, int y0 ) {
    const rgbTriple checkColor = *(canvas+(y0*getWidth()+x0));  // color to be checked against
    int xMax = getWidth();
    int yMax = getHeight();
    int x = x0;
    int y = y0;
    rgbTriple color;
    /* go along first horizontal line */
    while ( sameColorCoordinates(x, y0, checkColor ) && ! sameColorCoordinates(x, y0-1, checkColor ) && x < xMax ) {
        x++;
    }
    int x1 = x-1;

    /* go along first vertical line */
    while ( sameColorCoordinates(x0, y, checkColor ) && ! sameColorCoordinates(x0-1, y, checkColor ) && y < yMax ) {
        y++;
    }
    int y1 = y-1;

    /* go along second horizontal line */
    x = x0;
    while ( sameColorCoordinates(x, y1, checkColor ) && ! sameColorCoordinates(x, y1+1, checkColor ) && x < xMax ) {
        x++;
    }
    int x2 = x-1;

    /* go along first vertical line */
    y = y0;
    while ( sameColorCoordinates(x1, y, checkColor ) && ! sameColorCoordinates(x1+1, y, checkColor ) && y < yMax ) {
        y++;
    }
    int y2 = y-1;

    if ( x1 == x2 && y1 == y2 && x0 < x1 && y0 < y1 ) {
        for( y = y0; y <= y1; y++ ) {
            for( x = x0; x <= x1; x++ ) {
                if ( !sameColorCoordinates(x, y, checkColor) ) {
                    return(0);
                }
            }
        }
        rectangleInfo* tempRect = malloc(sizeof(rectangleInfo));
        *tempRect = (rectangleInfo){checkColor, x0, x1, y0, y1};
        return(tempRect);
    } else {
        return(0);
    }
}

rgbTriple* markRectangles ( void ) {
    canvas = getCanvasCopy();
    int xMax = getWidth();
    int yMax = getHeight();
    rectangleInfo* tempRect = 0;
    /* initalize array */
    int isRect[getWidth()][getHeight()];
    for ( int a = 0; a < xMax; a++ )
    for ( int b = 0; b < yMax; b++ ){
            isRect[a][b] = 0;
    }
    for ( int y = 0; y < yMax; y++ )
    for ( int x = 0; x < xMax; x++ ) {
        if ( !isRect[x][y] ) {
            tempRect = checkForRectangle(x, y);
            if ( tempRect != 0 ){
                /* draw border lines */
                drawHorizontalLine(tempRect->xMin, tempRect->xMax, tempRect->yMin);
                drawHorizontalLine(tempRect->xMin, tempRect->xMax, tempRect->yMax);
                drawVerticalLine(tempRect->yMin, tempRect->yMax, tempRect->xMin);
                drawVerticalLine(tempRect->yMin, tempRect->yMax, tempRect->xMax);
                /* mar as rectangle in array */
                for ( int a = tempRect->xMin; a <= tempRect->xMax; a++ )
                for ( int b = tempRect->yMin; b <= tempRect->yMax; b++ ){
                    isRect[a][b] = 1;
                }
                free(tempRect);
                tempRect = 0;
            }
        }
    }
    return(canvas);
}